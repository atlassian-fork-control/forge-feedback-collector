# Forge Feedback Collector Macro

This simple Forge app adds a form macro to your Confluence page that allows users to submit feedback that is converted into a Jira ticket for a project on the same site.

![Form](screenshots/app-initial.png)

![Form](screenshots/app-success.png)

## Quick start

### Install and deploy the app
 
You will need Node.js and the Forge CLI to install this app. You can install the Forge CLI by running `npm install -g @forge/cli`.

Once you have logged into the CLI (`forge login`), follow the steps below to install the app onto your site:

1. Clone this repository
2. Run `forge register` to register a new copy of this app to your developer account
3. Run `npm install` to install your dependencies.
4. Run `forge deploy` to deploy the app into the default environment
5. Run `forge install` and follow the prompts to install the app into your Confluence site

## Project overview

- `index.tsx`
Contains the main logic of the app

- `issue.tsx`
Contains helper methods to format the body of the Jira Cloud REST API request
